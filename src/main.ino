#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <DHT.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);
#define dht_apin A0 

int maxT=-50,minT=100,maxH=0,minH=100;
dht DHT;
 
void setup(){
 display.begin(SSD1306_SWITCHCAPVCC, 0x3C); 
}
 
void loop()
{
 
  DHT.read11(dht_apin);
  //resetting min max 
  if ((int)DHT.temperature>maxT) {maxT = (int)DHT.temperature;}
  if ((int)DHT.temperature<minT){minT = (int)DHT.temperature;}
  if ((int)DHT.humidity>maxH){maxH = (int)DHT.humidity;}
  if ((int)DHT.humidity<minH){minH = (int)DHT.humidity;}
  display.clearDisplay();
  //preparing layout
  display.drawLine(0,0,0,63,WHITE);
  display.drawLine(0,63,127,63,WHITE);
  display.drawLine(127,63,127,0,WHITE);
  display.drawLine(127,0,0,0,WHITE);
  display.drawLine(63,0,63,63,WHITE);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(6,4);   
  display.print("Umiditate");
  display.setCursor(84,4);   
  display.print("Temp");
    //min/max layouts
  display.drawLine(0,35,127,35,WHITE);
  display.drawLine(0,36,127,36,WHITE);
  display.drawLine(31,36,31,63,WHITE);    
  display.drawLine(96,36,96,63,WHITE);
    display.setCursor(8,40);   
    display.print("Min");
    display.setCursor(39,40);   
    display.print("Max");
    display.setCursor(71,40);   
    display.print("Min");
    display.setCursor(103,40);   
    display.print("Max");
  //writing actual values
  display.setCursor(17,16);
  display.setTextSize(2);
  display.print(DHT.humidity,0); 
  display.print("%");
  display.setCursor(78,16);   
  display.print(DHT.temperature,0);
  display.print("C");  
  //printing min/max
    display.setTextSize(1);
    display.setCursor(11,52);   
    display.print(minH);
    display.setCursor(42,52);   
    display.print(maxH);
    display.setCursor(73,52);   
    display.print(minT);
    display.setCursor(106,52);   
    display.print(maxT);
  //printing and waiting
  display.display();
  delay(10000);

 
}// end loop() 